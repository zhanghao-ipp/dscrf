#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Detection of Species, Chemotype and Fungicide Resistance of Fusarium

import argparse
import subprocess
import copy
from multiprocessing import Pool
from collections import defaultdict
parser = argparse.ArgumentParser(description='DSCRF')

parser.add_argument('-1', '--read1', required=True, type=str, help='Mate 1s reads file for analysing (can be gzipped)')
parser.add_argument('-2', '--read2', required=True, type=str, help='Mate 1s reads file for analysing (can be gzipped)')
parser.add_argument('-d', '--dir', type=str, required=False, default='DFRB', help='Output directory for all output files.')
parser.add_argument('-t', '--threads', type=int, required=False, default=1, help='Number of threads used.')
parser.add_argument('-o', '--output', type=str, required=False, default='identification.txt', help='Output file name')
args = parser.parse_args()

def run_command(command, **kwargs):
    command_str = ' '.join(command)
    subprocess.call(command_str, **kwargs)

def get_vcf(strain): 
    run_command(["bwa", "mem", "ref3.fasta", args.dir + "/Demultiplexing/" + strain + "_R1.fastq", args.dir + "/Demultiplexing/" + strain + "_R2.fastq", ">", args.dir + "/tmp/" + strain + ".sam"], shell=True)
    run_command(["samtools", "view", "-buS", args.dir + "/tmp/" + strain + ".sam", "|", "samtools", "sort", "-o", args.dir + "/tmp/" + strain + ".sort.bam"], shell=True)
    run_command(["bedtools", "-genomecov", "-ibam", args.dir + "/tmp/" + strain + ".sort.bam", "-d", ">", args.dir + "/cov/" + strain + ".cov"], shell=True)
    run_command(["freebayes", "-f", "ref3.fasta", args.dir + "/tmp/" + strain + ".sort.bam", "-p", "1", "|", "vcffilter", "-f", "'DP > 50 & QUAL > 50'", ">", args.dir + "/vcf/" + strain + ".vcf"], shell=True)

def wrap_vcf(strain, F):
    var = copy.deepcopy(F)
    with open(args.dir + "/vcf/" + strain + ".vcf", 'r') as f1:
        for line in f1:
            if not line.startswith('#'):
                line = line.strip().split()
                pos = int(line[1])
                if len(line[3]) == len(line[4]):
                    for i in line[4]:
                        var[pos] = i
                        pos = pos + 1
                else:
                    var[pos] = line[4][0]
                    var[pos + len(line[3]) -1] = line[4][-1]
    with open(args.dir + "/cov/" + strain + ".cov", 'r') as f2:
        for line in f2:
            line = line.strip().split()
            if int(line[2]) < 50:
                var[int(line[1])] = "none"
    return var

def identify(var):
    if var[221] == "C":
        if var[55] == "A" and var[133] == "C":
            sp = 'F. asiaticum'
        elif var[55] == "A" and var[133] == "T" and var[134] == "A" and var[182] == "A":
            sp = 'F. asiaticum'
        elif var[171] == "C":
            sp = "F. graminearum"
        elif var[233] == "T":
            sp = "F. austroamericanum"
        elif var[24] == "T":
            sp = "F. meridionale"
        elif var[191] == "T":
            sp = "F. ussurianum"
        elif var[97] == "T":
            sp = "F. nepalense"
        elif var[180] == "T":
            sp = "F. louisianense"
        else:
            sp = "FGSC other species"
    else:
        if var[189] == "T" and var[98] == "A":
            sp = "F. pseudograminearum"
        elif var[197] == "A":
            sp = "F. culmorum"
        elif var[45] == "C" and var[205] == "A":
            sp = "F. poae"
        elif var[207] == "C" and var[198] == "G":
            sp = "F. cerealis"
        else:
            sp = "unknown species"
    if var[364] == "T":
        ch = "15ADON"
    elif var[292] == "C" and var[382] == "T":
        ch = "3ADON"
    elif var[316] == "C" and var[349] == "G":
        ch = "NIV"
    else:
        ch = "unknown chemotype"
    if var[690] == "A":
        res = "F167Y"
    elif var[782] == "C" and var[783] == "A":
        res = "E198Q"
    elif var[782] == "C" and var[783] == "T":
        res = "E198L"
    elif var[690] == "A":
        res = "F200Y"
    else:
        res = "sensitive"
    return sp, ch, res

if __name__ == '__main__':
    F = {55:"C", 133:"C", 134:"G", 182:"C", 171:"C", 221:"C", 233:"C", 24:"C", 191:"A", 97:"G", 180:"T", 189:"C", 98:"T", 197:"T", 45:"T", 205:"C", 207:"T", 198:"G", 292:"G", 328:"C", 364:"T", 316:"A", 349:"A", 690:"T", 782:"G", 783:"A", 789:"T"}
    run_command(["mkdir", args.dir], shell=True)
    run_command(["mkdir", args.dir + "/Demultiplexing"], shell=True)
    run_command(["mkdir", args.dir + "/tmp"], shell=True)
    run_command(["mkdir", args.dir + "/vcf"], shell=True)
    run_command(["mkdir", args.dir + "/cov"], shell=True)
    run_command(["mkdir", args.dir + "/results"], shell=True)
    run_command(["seqtk_demultiplex", "-b", "barcode_seqtk.txt", "-1", args.read1, "-2", args.read2, "-d", args.dir + "/Demultiplexing"], shell=True)
    strains_list = []
    for line in open("barcode_seqtk.txt"):
        line = line.strip().split()
        strains_list.append(line[0])
    pool = Pool(args.threads)                         # Create a multiprocessing Pool
    pool.map(get_vcf, strains_list)  # proces strains_list iterable with pool
    with open(args.dir + "/identification.txt", 'w') as w1:
        w1.write("strains\tspecies\tchemotype\tresistance\n" )
        for i in strains_list:
            variation = wrap_vcf(i, F)
            species,chemotype,resistance = identify(variation)
            w1.write(i + "\t" + species + "\t" + chemotype + "\t" + resistance + "\n")
